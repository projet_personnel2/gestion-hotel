// importation des modules
const express = require("express");
const cors = require("cors");
const port = process.env.PORT || 3001;
const authRouter = require("./routes/authRouter");
const userRouter = require("./routes/userRouter");
const chambreRouter = require("./routes/chambreRouter");
const categorieChambreRouter = require("./routes/categorieChambreRouter");
const serviceRouter = require("./routes/serviceRouter");

// initialisation de l'api

const app = express();

// Importez la fonction dbConnect
const dbConnect = require("./config/dbconnect"); // Assurez-vous que le chemin du fichier est correct
const checkTokenMiddleware = require("./middleware/check");
const errorHandler = require("./error/errorHandler");

// Appel de la fonction dbConnect pour établir la connexion à la base de données
dbConnect();

app.use(
  cors({
    origin: "*",
    methods: ["GET", "POST", "PUT", "PATCH", "DELETE"],
    allowedHeaders:
      "Origin, X-Requested-With, x-access-token, role, Content, Accept, Content-Type, Authorization",
  })
);
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Middleware pour logger les dates de requetes
app.use((req, res, next) => {
  const event = new Date();
  console.log("Times request ", event.toString());
  next();
});

// routes
app.get("/", (req, res) => res.send(`i'm online`));

app.use("/api/auth", authRouter);
app.use("/api/user", checkTokenMiddleware, userRouter);
app.use("/api/chambre", chambreRouter);
app.use("/api/categorie-chambre", categorieChambreRouter);
app.use("/api/service", serviceRouter);

app.get("*", (req, res) => res.status(501).send("Page inexistante"));

app.use(errorHandler);

// Demarrer le serveur

app.listen(port, () => {
  console.log(`Serveur Express écoutant sur le port ${port}`);
});
