const errorHandler = (err, req, res, next) => {
  console.log("Je suis dans le handllerError");
    // Niveau de débogage (0 - Message simple, 1 - Message sans erreur, 2 - Toutes les informations)
    const debugLevel = 1;
    
    let message;
  
    switch (debugLevel) {
      case 0:
        message = { message: err.message };
        break;
      case 1:
        message = { message: err.message };
        break;
      case 2:
        message = { message: err.message, error: err };
        break;
      default:
        console.log('Mauvais niveau de débogage');
    }
  
    if (err.name === 'ValidationError') {
      // Gérer les erreurs de validation Mongoose
      return res.status(400).json(message);
    } else if (err.name === 'CastError') {
      // Gérer les erreurs de conversion de type Mongoose
      return res.status(400).json(message);
    } else if (err.name === 'MongoError' && err.code === 11000) {
      // Gérer les erreurs d'index unique (doublon) Mongoose
      return res.status(400).json({ message: 'Ce champ doit être unique.' });
    } else {
      // Gérer d'autres erreurs Mongoose
      return res.status(500).json(message);
    }
  };
  
  module.exports = errorHandler;
  