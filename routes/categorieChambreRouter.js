const express = require("express");
const { createCategorieChambre, getAllCategoriesChambres, getCategorieChambreById, updateCategorieChambreById, deleteCategorieChambreById } = require("../controllers/categorieChambreController");
const checkTokenMiddleware = require("../middleware/check");

const Router = express.Router();

Router.post("/create", checkTokenMiddleware, createCategorieChambre);
Router.get("/all", getAllCategoriesChambres);
Router.get("/:id", getCategorieChambreById);
Router.patch("/edit/:id",checkTokenMiddleware, updateCategorieChambreById);
Router.delete("/delete/:id",checkTokenMiddleware, deleteCategorieChambreById);

module.exports = Router;