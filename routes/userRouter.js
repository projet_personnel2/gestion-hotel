const express = require("express");
const {
  createUser,
  getUsers,
  getUserById,
  updateUserById,
  deleteUserById,
  softDeleteUser,
  softActifUser,
} = require("../controllers/userController");


const Router = express.Router();

// Routes utilisateur en utilisant les fonctions du contrôleur
Router.post("/create", createUser);

Router.get("/all", getUsers);
Router.get("/user/:id", getUserById);
Router.patch("/edit/:id", updateUserById);
Router.put('/restore/:id', softActifUser);
Router.delete('/trash-delete/:id', softDeleteUser);
Router.delete("/delete/:id", deleteUserById);

module.exports = Router;
