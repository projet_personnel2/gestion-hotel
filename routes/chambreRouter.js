const express = require("express");
const {
  createChambre,
  getChambres,
  getChambreById,
  updateChambreById,
  deleteChambreById,
} = require("../controllers/chambreController");
const checkTokenMiddleware = require("../middleware/check");

const Router = express.Router();

// Routes de chambre en utilisant les fonctions du contrôleur de chambre
Router.post("/create-chambre",checkTokenMiddleware, createChambre);
Router.get("/all-chambres", getChambres);
Router.get("/chambre/:id", getChambreById);
Router.patch("/chambre-edit/:id",checkTokenMiddleware, updateChambreById);
Router.delete("/chambre-delete/:id",checkTokenMiddleware, deleteChambreById);

module.exports = Router;
