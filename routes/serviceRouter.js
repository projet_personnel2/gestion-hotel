const express = require("express");

const checkTokenMiddleware = require("../middleware/check");
const { createService, getAllServices, getServiceById, updateServiceById, deleteServiceById } = require("../controllers/serviceController");

const Router = express.Router();

// Routes de chambre en utilisant les fonctions du contrôleur de chambre
Router.post("/create",checkTokenMiddleware, createService);
Router.get("/all", getAllServices);
Router.get("/:id", getServiceById);
Router.patch("/edit/:id",checkTokenMiddleware, updateServiceById);
Router.delete("/delete/:id",checkTokenMiddleware, deleteServiceById);

module.exports = Router;
