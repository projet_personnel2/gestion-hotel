const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator"); // Module pour gérer les champs uniques
const bcrypt = require("bcrypt");

const userSchema = new mongoose.Schema(
  {
    nom: {
      type: String,
      required: true,
    },
    prenom: {
      type: String,
      required: true,
    },
    pseudo: {
      type: String,
      required: true,
      unique: true,
    },
    email: {
      type: String,
      required: true,
      unique: true, // Déclarer le champ comme unique
    },
    mobile: {
      type: String,
      required: true,
      unique: true, // Déclarer le champ comme unique
    },
    password: {
      type: String,
      required: true,
    },
    statut: {
      type: String,
      enum: ['actif', 'supprimé', 'inactif'],
      default: 'actif', // Par défaut, les utilisateurs sont actifs
    },
  },
  {
    timestamps: true,
  }
);

// Appliquer le plugin uniqueValidator pour gérer les erreurs de validation unique
userSchema.plugin(uniqueValidator);

// Hachage du mot de passe avant de sauvegarder l'utilisateur
userSchema.pre("save", async function (next) {
  try {
    const salt = await bcrypt.genSalt(parseInt(process.env.BCRYPT_SALT_ROUND));
    const hashedPassword = await bcrypt.hash(this.password, salt);
    this.password = hashedPassword;
    next();
  } catch (error) {
    next(error);
  }
});

// Méthode pour vérifier le mot de passe
userSchema.methods.isValidPassword = async function (password) {
  try {
    return await bcrypt.compare(password, this.password);
  } catch (error) {
    throw error;
  }
};

const User = mongoose.model("User", userSchema);

module.exports = User;
