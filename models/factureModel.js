const mongoose = require('mongoose');

const factureSchema = new mongoose.Schema({
  reservation_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Reservation',
  },
  montant_total_chambre: Number, // Montant total de la chambre
  montant_total_services: Number, // Montant total des services
  montant_total: Number, // Montant total de la facture (chambre + services)
  date_facturation: Date,
  payee: Boolean,
});

const Facture = mongoose.model('Facture', factureSchema);

module.exports = Facture;
