const mongoose = require('mongoose');

const reservationSchema = new mongoose.Schema({
  client_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Client',
  },
  chambre_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Chambre',
  },
  date_arrivee: Date,
  date_depart: Date,
  total_paiement: Number,
  services: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Service',
  }],
});

const Reservation = mongoose.model('Reservation', reservationSchema);

module.exports = Reservation;
