const mongoose = require("mongoose");

const categorieChambreSchema = new mongoose.Schema(
  {
    nom: {
      type: String,
      required: true,
      unique: true,
    },
  },
  {
    timestamps: true,
  }
);

const CategorieChambre = mongoose.model(
  "CategorieChambre",
  categorieChambreSchema
);

module.exports = CategorieChambre;
