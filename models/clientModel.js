const mongoose = require('mongoose');

const clientSchema = new mongoose.Schema({
  nom: String,
  email: String,
  num_tel: String,
  adresse: String,
  ville: String,
  pays: String,
});

const Client = mongoose.model('Client', clientSchema);

module.exports = Client;
