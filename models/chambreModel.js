const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator"); // Module pour gérer les champs uniques


const chambreSchema = new mongoose.Schema(
  {
    numero: {
      type: String,
      required: true,
      unique: true,
      lowercase: true,
    },
    prix_nuit: {
      type: Number,
      required: true,
    },
    categorie: {
      type: String,
      required: true,
      default: "chambre",
    },
    disponibilite: {
      type: String,
      enum: ["disponible", "occupe", "ferme"],
      default: "disponible",
    },

    // categorie: {
    //   type: mongoose.Schema.Types.ObjectId,
    //   ref: "Categorie",
    //   required : true
    // },
  },
  {
    timestamps: true,
  }
);

// Appliquer le plugin uniqueValidator pour gérer les erreurs de validation unique
chambreSchema.plugin(uniqueValidator);

const Chambre = mongoose.model("Chambre", chambreSchema);

module.exports = Chambre;
