const { ServiceError, RequestError } = require("../error/customError");
const Service = require("../models/serviceModel");

// Fonction pour créer un nouveau service
const createService = async (req, res, next) => {
  try {
    const { nom, description, prix } = req.body;

    // Vérifier si un service avec le même nom existe déjà
    const existingService = await Service.findOne({ nom });

    if (existingService) {
      throw new ServiceError("Ce service existe déjà");
    }

    // Créer un nouveau service
    const service = new Service({ nom, description, prix });
    await service.save();

    res.status(201).json(service);
  } catch (error) {
    next(error);
  }
};

// Fonction pour récupérer tous les services
const getAllServices = async (req, res, next) => {
  try {
    const services = await Service.find();
    res.status(200).json(services);
  } catch (error) {
    next(error);
  }
};

// Fonction pour récupérer un service par son ID
const getServiceById = async (req, res, next) => {
  try {
    const serviceId = req.params.id;
    if (!parseInt(serviceId)) {
      throw new RequestError("Mauvais Parametre");
    }
    const service = await Service.findById(serviceId);
    if (!service) {
      throw new ServiceError("Service non trouvé");
    }
    res.status(200).json(service);
  } catch (error) {
    next(error);
  }
};

// Fonction pour mettre à jour un service par son ID
const updateServiceById = async (req, res, next) => {
  try {
    const { nom, description, prix } = req.body;
    const serviceId = req.params.id;
    if (!parseInt(serviceId)) {
      throw new RequestError("Mauvais Parametre");
    }

    // Vérifier si un service avec le même nom existe déjà (sauf pour le service actuel)
    const existingService = await Service.findOne({
      nom,
      _id: { $ne: serviceId },
    });

    if (existingService) {
      throw new ServiceError("Ce service existe déjà");
    }

    // Mettre à jour le service
    const service = await Service.findByIdAndUpdate(
      serviceId,
      { nom, description, prix },
      { new: true }
    );

    if (!service) {
      throw new ServiceError("Service non trouvé");
    }

    res.status(200).json(service);
  } catch (error) {
    next(error);
  }
};

// Fonction pour supprimer un service par son ID
const deleteServiceById = async (req, res, next) => {
  try {
    const serviceId = req.params.id;
    if (!parseInt(serviceId)) {
      throw new RequestError("Mauvais Parametre");
    }
    const service = await Service.findByIdAndRemove(serviceId);
    if (!service) {
      throw new ServiceError("Service non trouve");
    }
    res.status(204).json({message:"Service supprime"});
  } catch (error) {
    next(error);
  }
};

module.exports = {
  createService,
  getAllServices,
  getServiceById,
  updateServiceById,
  deleteServiceById,
};
