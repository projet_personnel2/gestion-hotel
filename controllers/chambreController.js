// import des ressources neccessaires
// const { RequestError, ChambreError } = require("../error");
const {
  RequestError,
  ChambreError,
} = require("../error/customError");
const Chambre = require("../models/chambreModel");
const mongoose = require("mongoose");

// Fonction pour créer une nouvelle chambre
const createChambre = async (req, res,next) => {
  try {
    const chambre = new Chambre(req.body);
    await chambre.save();
    res.status(201).send(chambre);
  } catch (error) {
    if (error.name === "ValidationError" && error.errors) {
      // Gérer les erreurs de validation unique (numero en double)
      if (error.errors.numero && error.errors.numero.kind === "unique") {
        return res
          .status(400)
          .send(
            "Ce numero de chambre est déjà utilisé par un autre utilisateur."
          );
        // throw new CreateChambreError("Ce numero de chambre est déjà utilisé par un autre utilisateur.")
      }
    }
    next(error)
  }
};

// Fonction pour obtenir la liste de toutes les chambres
const getChambres = async (req, res, next) => {
  try {
    const chambres = await Chambre.find();
    res.status(200).send(chambres);
  } catch (error) {
    next(error);
  }
};

// Fonction pour obtenir une chambre par son ID
const getChambreById = async (req, res, next) => {
  try {
    const chambreId = req.params.id;
    if (!parseInt(chambreId)) {
      throw new RequestError("Mauvais Parametre");
    }
    const chambre = await Chambre.findOne({
      _id: new mongoose.Types.ObjectId(chambreId),
    });
    if (!chambre) {
      throw new ChambreError("Chambre introuvable ou inexistent", 0);
    }
    res.status(200).send(chambre);
  } catch (error) {
    // console.log("dans le catch : ");
    // console.log(`####nom : ${error.name}`);
    // console.log(`####status : ${error.statusCode}`);
    // console.log(`####status : ${error.message}`);

    next(error);
  }
};

// Fonction pour mettre à jour une chambre par son ID
const updateChambreById = async (req, res,next) => {
  const updates = Object.keys(req.body);
  const allowedUpdates = [
    "numero",
    "type",
    "prix_nuit",
    "disponibilite",
    "categorie",
  ];
  const isValidOperation = updates.every((update) =>
    allowedUpdates.includes(update)
  );

  if (!isValidOperation) {
    throw new ChambreError('Mise à jour invalide')
    // return res.status(400).send("Mise à jour invalide");
  }

  try {
    // Récupérer la chambre existante par son ID
    const chambreId = req.params.id;
    if (!parseInt(chambreId)) {
      throw new RequestError("Mauvais Parametre");
    }
    const chambre = await Chambre.findById(chambreId);
    if (!chambre) {
      throw new ChambreError("Chambre introuvable ou inexistent", 0);
      // return res.status(404).send("Chambre introuvable");
    }

    // Vérifier si le champ "numero" existe dans les mises à jour
    if (req.body.numero && req.body.numero !== chambre.numero) {
      // Vérifier si une autre chambre a déjà ce numéro
      const existingChambreWithNumero = await Chambre.findOne({
        numero: req.body.numero,
      });
      if (existingChambreWithNumero) {
        throw new ChambreError("Ce numéro de chambre est déjà utilisé par une autre chambre.", 0);
        // return res
        //   .status(400)
        //   .send("Ce numéro de chambre est déjà utilisé par une autre chambre.");
      }
    }

    // Mettre à jour la chambre avec les données du corps de la demande
    updates.forEach((update) => {
      chambre[update] = req.body[update];
    });

    await chambre.save();
    res.status(200).send(chambre);
  } catch (error) {
    next(error)
  }
};

// Fonction pour supprimer une chambre par son ID
const deleteChambreById = async (req, res, next) => {
  try {
    const chambreId = req.params.id;
    console.log(`chambre id : ${chambreId}`);
    if (!parseInt(chambreId)) {
      throw new RequestError("Mauvais Parametre");
    }

    const chambre = await Chambre.findByIdAndDelete(chambreId);
    if (!chambre) {
      throw new ChambreError("Chambre introuvable ou inexistent", 0);
    }
    res.status(204).send();
  } catch (error) {
    next(error);
  }
};

module.exports = {
  createChambre,
  getChambres,
  getChambreById,
  updateChambreById,
  deleteChambreById,
};
