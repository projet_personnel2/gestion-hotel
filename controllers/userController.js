// controllers/userController.js
const { RequestError, UserError, AuthenticationError } = require("../error/customError");
const User = require("../models/userModel");
var jwt = require("jsonwebtoken");

// Contrôleur pour créer un nouvel utilisateur
const createUser = async (req, res, next) => {
  try {
    const user = new User(req.body);
    await user.save();
    res.status(201).send(user);
  } catch (error) {
    if (error.name === "ValidationError") {
      // Gérer les erreurs de validation unique (email ou mobile en double)
      if (error.errors.email && error.errors.email.kind === "unique") {
        return res
          .status(400)
          .send("Cet email est déjà utilisé par un autre utilisateur.");
      }
      if (error.errors.mobile && error.errors.mobile.kind === "unique") {
        return res
          .status(400)
          .send(
            "Ce numéro de mobile est déjà utilisé par un autre utilisateur."
          );
      }
      // Gérer les erreurs de validation
      const validationErrors = Object.values(error.errors).map(
        (error) => error.message
      );
      return res.status(400).json("formulaire " + validationErrors);
    }
    res.status(400).send(error); // Autres erreurs
  }
};

// Fonction pour obtenir la liste de tous les utilisateurs
const getUsers = async (req, res, next) => {
  try {
    const users = await User.find();
    res.status(200).send(users);
  } catch (error) {
    next(error);
  }
};

// Fonction pour obtenir un utilisateur par son ID
const getUserById = async (req, res, next) => {
  try {
    const userId = req.params.id;
    if (!parseInt(userId)) {
      throw new RequestError("Mauvais Parametre");
    }
    const user = await User.findById(userId);
    if (!user) {
      throw new UserError("Utilisateur introuvable");
    }
    res.status(200).send(user);
  } catch (error) {
    next(error);
  }
};

// Contrôleur pour mettre à jour un utilisateur par son ID
const updateUserById = async (req, res, next) => {
  const updates = Object.keys(req.body);
  const allowedUpdates = [
    "nom",
    "prenom",
    "pseudo",
    "email",
    "mobile",
    "password",
  ];
  const isValidOperation = updates.every((update) =>
    allowedUpdates.includes(update)
  );

  if (!isValidOperation) {
    throw new UserError("Mise à jour invalide");
  }

  try {
    const userId = req.params.id;
    if (!parseInt(userId)) {
      throw new RequestError("Mauvais Parametre");
    }
    const user = await User.findById(userId);
    if (!user) {
      throw new UserError("Utilisateur introuvable");
    }

    // Vérification si le champ "email" existe dans les mises à jour
    if (req.body.email) {
      // Vérifier si un autre utilisateur a déjà cet email
      const existingUserWithEmail = await User.findOne({
        email: req.body.email,
      });
      if (
        existingUserWithEmail &&
        existingUserWithEmail._id.toString() !== req.params.id
      ) {
        throw new UserError(
          "Cet email est déjà utilisé par un autre utilisateur."
        );
      }
    }

    // Vérification si le champ "mobile" existe dans les mises à jour
    if (req.body.mobile) {
      // Vérifier si un autre utilisateur a déjà ce numéro de mobile
      const existingUserWithMobile = await User.findOne({
        mobile: req.body.mobile,
      });
      if (
        existingUserWithMobile &&
        existingUserWithMobile._id.toString() !== req.params.id
      ) {
        throw new UserError(
          "Ce numéro de mobile est déjà utilisé par un autre utilisateur."
        );
      }
    }

    // Mettre à jour l'utilisateur
    updates.forEach((update) => {
      user[update] = req.body[update];
    });
    await user.save();

    res.status(200).send(user);
  } catch (error) {
    next(error);
  }
};

// Contrôleur pour modifier le statut à supprimé de l'utilisateur  par son ID
const softDeleteUser = async (req, res, next) => {
  try {
    const userId = req.params.id;
    if (!parseInt(userId)) {
      throw new RequestError("Mauvais Parametre");
    }

    // Recherchez l'utilisateur par son ID
    const user = await User.findById(userId);

    if (!user) {
      throw new UserError("Utilisateur introuvable");
    }

    // Mettez à jour le statut de l'utilisateur à "supprimé"
    user.statut = "supprimé";
    await user.save();

    res.status(200).send("Utilisateur marqué comme supprimé avec succès");
  } catch (error) {
    next(error);
  }
};

// Contrôleur pour modifier le statut à actif de l'utilisateur  par son ID
const softActifUser = async (req, res, next) => {
  try {
    const userId = req.params.id;
    if (!parseInt(userId)) {
      throw new RequestError("Mauvais Parametre");
    }

    // Recherchez l'utilisateur par son ID
    const user = await User.findById(userId);

    if (!user) {
      throw new UserError("Utilisateur introuvable");
    }

    // Mettez à jour le statut de l'utilisateur à "actif"
    user.statut = "actif";
    await user.save();

    res.status(200).send("Utilisateur restauré avec succès");
  } catch (error) {
    res.status(500).send(error);
  }
};

// Fonction pour supprimer un utilisateur par son ID
const deleteUserById = async (req, res, next) => {
  try {
    const userId = req.params.id;
    if (!parseInt(userId)) {
      throw new RequestError("Mauvais Parametre");
    }
    const user = await User.findByIdAndDelete(userId);
    if (!user) {
      throw new UserError("Utilisateur introuvable");
    }
    res.status(204).send();
  } catch (error) {
    next(error);
  }
};

// Contrôleur pour vérifier le mot de passe lors de la connexion
const login = async (req, res, next) => {
  try {
    const { email, password } = req.body;

    // Rechercher l'utilisateur par son email
    const user = await User.findOne({ email });

    if (!user) {
      throw new AuthenticationError('Utilisateur non trouvé',1)
    }

    // Utiliser la méthode isValidPassword pour vérifier le mot de passe
    const passwordMatch = await user.isValidPassword(password);

    if (!passwordMatch) {
      throw new AuthenticationError('Mot de passe incorrect',1)
    }

    // Authentification réussie, vous pouvez générer un jeton JWT ou une session ici
    const token = jwt.sign(
      {
        id: user._id,
        nom: user.nom,
        prenom: user.prenom,
        pseudo: user.pseudo,
        email: user.email,
        mobile: user.mobile,
      },
      process.env.JWT_TOKEN_SECRET,
      { expiresIn: process.env.JWT_DURING }
    );

    if (!token) {
      throw new AuthenticationError('Erreur lors de la génération du token')
      // return res
      //   .status(500)
      //   .json({ error: "Erreur lors de la génération du token" });
    }

    res.json({ access_token: token });
  } catch (error) {
    if (error.name === "ValidationError") {
      // Gérer les autres erreurs de validation
      const validationErrors = Object.values(error.errors).map(
        (error) => error.message
      );
      return res
        .status(400)
        .json({ error: "Formulaire " + validationErrors.join(", ") });
    }
    next(error);
  }
};

module.exports = {
  createUser,
  getUsers,
  getUserById,
  updateUserById,
  deleteUserById,
  login,
  softDeleteUser,
  softActifUser,
};
