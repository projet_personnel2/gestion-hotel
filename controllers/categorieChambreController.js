const { CategorieChambreError } = require("../error/customError");
const CategorieChambre = require("../models/categoryChambreModel");

// Fonction pour créer une nouvelle catégorie de chambre
const createCategorieChambre = async (req, res, next) => {
  try {
    const { nom } = req.body;
    // Vérifier si une catégorie avec le même nom existe déjà
    const existingCategorieChambre = await CategorieChambre.findOne({ nom });

    if (existingCategorieChambre) {
      throw new CategorieChambreError(`la catégorie ${nom} existe déjà`);
    }
    const categorieChambre = new CategorieChambre({ nom });
    await categorieChambre.save();
    res.status(201).json(categorieChambre);
  } catch (error) {
    next(error);
  }
};

// Fonction pour récupérer toutes les catégories de chambres
const getAllCategoriesChambres = async (req, res, next) => {
  try {
    const categoriesChambres = await CategorieChambre.find();
    res.status(200).json(categoriesChambres);
  } catch (error) {
    next(error);
  }
};

// Fonction pour récupérer une catégorie de chambre par son ID
const getCategorieChambreById = async (req, res, next) => {
  try {
    const categorieId = req.params.id;
    if (!parseInt(categorieId)) {
      throw new RequestError("Mauvais Parametre");
    }
    const categorieChambre = await CategorieChambre.findById(categorieId);
    if (!categorieChambre) {
      throw new CategorieChambreError("Catégorie de chambre non trouvée");
    }
    res.status(200).json(categorieChambre);
  } catch (error) {
    next(error);
  }
};

// Fonction pour mettre à jour une catégorie de chambre par son ID
const updateCategorieChambreById = async (req, res, next) => {
  try {
    const categorieId = req.params.id;
    if (!parseInt(categorieId)) {
      throw new RequestError("Mauvais Parametre");
    }
    const { nom } = req.body;
    // Vérifier si une catégorie avec le même nom existe déjà (sauf pour la catégorie actuelle)
    const existingCategorieChambre = await CategorieChambre.findOne({
      nom,
      _id: { $ne: categorieId },
    });

    if (existingCategorieChambre) {
      throw new CategorieChambreError("Cette catégorie de chambre existe déjà");
    }
    const categorieChambre = await CategorieChambre.findByIdAndUpdate(
      categorieId,
      { nom },
      { new: true }
    );
    if (!categorieChambre) {
      throw new CategorieChambreError("Catégorie de chambre non trouvée");
    }
    res.status(200).json(categorieChambre);
  } catch (error) {
    next(error);
  }
};

// Fonction pour supprimer une catégorie de chambre par son ID
const deleteCategorieChambreById = async (req, res, next) => {
  try {
    const categorieId = req.params.id;
    if (!parseInt(categorieId)) {
      throw new RequestError("Mauvais Parametre");
    }
    // const categorieChambreExist = await CategorieChambre.findById(categorieId);
    // if (!categorieChambreExist) {
    //   throw new CategorieChambreError("Catégorie de chambre non trouvée");
    // }

    const categorieChambre = await CategorieChambre.findByIdAndRemove(
      categorieId
    );
    if (!categorieChambre) {
      throw new CategorieChambreError("Catégorie de chambre non trouvée");
    }

    res.status(204).json({ message: "Catégorie de chambre a ete supprime" });
  } catch (error) {
    next(error)
  }
};

module.exports = {
  createCategorieChambre,
  getAllCategoriesChambres,
  getCategorieChambreById,
  updateCategorieChambreById,
  deleteCategorieChambreById,
};
