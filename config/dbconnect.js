const mongoose = require("mongoose");

const dbConnect = async () => {
  try {
    // Ajoutez une temporisation avant de tenter la connexion
    await new Promise((resolve) => setTimeout(resolve, 2000)); // Attendez 2 secondes
    await mongoose.connect(process.env.MONGODB_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      family: 4,
    });
    console.log("Database connected successfully");
  } catch (error) {
    console.error("Database error:", error.message);
  }
};

module.exports = dbConnect;
