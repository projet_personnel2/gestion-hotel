<div align="center">

#  Back End de la gestion d'un hotel ExpressJS 4
</div>

# A propos
Ce projet est le back de l'applicatif Gestion d'hotel 


## Fonctionnalitées
- Gestion des utilisateur compte et connection. 
- Gestion des Gestion d'hotel  en CRUD
- Session utilisatuer gérée avec le JsonWebToken




# Installation
Une fois le dépôt cloné et une fois rendu dans le dossier du projet ne pas oublier d'installer les dépendances
``` 
npm run server 
```

## Lancement

Ce projet démarre de 2 façon possible. Mode production et mode développement

```
npm run start
npm run dev
```