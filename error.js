class MainError extends Error {
  constructor(errorMessage) {
    super();

    this.name = this.constructor.name;
    this.message = errorMessage;

    if (this instanceof RequestError) {
      (this.statusCode = 400), (this.type = "request");
    }
    if (this instanceof ChambreError) {
      (this.statusCode = 404), (this.type = "chambre");
    }
  }
}

class RequestError extends MainError {}
class ChambreError extends MainError {}

module.exports = { MainError, RequestError, ChambreError };
